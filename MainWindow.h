#ifndef __MAIN_WINDOW_H__
#define __MAIN_WINDOW_H__

#include <QtWidgets>

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QLineEdit>
#include <QVector>

#include "qcustomplot.h"

#include "ELement.h"

class MainWindow: public QMainWindow {
    Q_OBJECT

    public:
        MainWindow(QWidget* parent = nullptr);
        ~MainWindow(); 
        int scanTable();

    public slots:
        void changeSpinBox(int); 
        void solve();
        void showGraphic();

    private:

        QMainWindow* window;
        QWidget* widget;
        QVBoxLayout* layout;
        QHBoxLayout* body;
        QHBoxLayout* header;
        QHBoxLayout* footer;

        QLabel* label;
        QLabel* labelInput;
        QLabel* labelAnsver;
        
        QDoubleSpinBox* spinBoxInput;
        QLineEdit* lineAnsver;
    
        QSpinBox* spinBox;

        QPushButton* btnShowGraphic;
        QPushButton* btnSolve;

        QTableWidget* table;
        QTableWidgetItem* itemX;
        QTableWidgetItem* itemY;

        QVector<Element>* elements;

        QCustomPlot* wGraphic;

};


#endif
