#ifndef __ELEMENT_H__
#define __ELEMENT_H__

class Element {
    private:
        int x;
        double px;
        double dpx;

    public:
        Element() {
        }

        Element(int _x) {
            x = _x;
        }
        Element(double _px) {
            px = _px;
        }

        int getX() {
            return x;
        }

        double getPx() {
            return px;
        }

        void setX(int _x) {
            x = _x;
        }

        void setPx(double _px) {
            px = _px;
        }

        void setDpx(double _dpx) {
            dpx = _dpx;
        }

        double getDpx() {
            return dpx;
        }
};

#endif
