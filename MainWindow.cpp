#include "MainWindow.h"
#include <iostream>
#include <cmath>

#include <QMessageBox>

#include "qcustomplot.h"

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) { widget = new QWidget();
    layout = new QVBoxLayout();

    header = new QHBoxLayout();
    body = new QHBoxLayout();    
    footer = new QHBoxLayout();
    
    btnShowGraphic = new QPushButton(); 
    btnShowGraphic->setText("Показати графік");

    spinBox = new QSpinBox();
    spinBox->setMinimum(1);

    btnSolve = new QPushButton();
    btnSolve->setText("Розв'язати");

    table = new QTableWidget();

    table->setRowCount(2);
    table->setColumnCount(spinBox->value());

    elements = new QVector<Element>;

    labelInput = new QLabel();
    labelInput->setText("x = ");

    spinBoxInput = new QDoubleSpinBox();
    
    labelAnsver = new QLabel();
    labelAnsver->setText("Відповідь: ");

    lineAnsver = new QLineEdit();
    lineAnsver->setReadOnly(true);

    wGraphic = new QCustomPlot();

    connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(changeSpinBox(int)));
    connect(btnSolve, SIGNAL(clicked()), this, SLOT(solve()));
    connect(btnShowGraphic, SIGNAL(clicked()), this, SLOT(showGraphic()));

    //Оформлення
    this->setWindowTitle("Дискретний розподіл випадкової величини");
    this->setMinimumSize(700, 500);

    layout->setMargin(10);

    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
//    table->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);


    //Позиціювання
    header->addWidget(spinBox);
    header->addWidget(btnSolve);
    header->addWidget(btnShowGraphic);
    
    body->addWidget(table);

    footer->addWidget(labelInput);
    footer->addWidget(spinBoxInput);
    footer->addWidget(labelAnsver, 100, Qt::AlignRight);
    footer->addWidget(lineAnsver, 20, Qt::AlignRight);

    layout->addLayout(header);
    layout->addLayout(body);
    layout->addLayout(footer);

    widget->setLayout(layout);
    setCentralWidget(widget);

}

void MainWindow::solve() {
    if(!scanTable()) {
        std::cout << "помилка\n" ;
        return;
    }
   
    double answer = 0;

    for(auto it = elements->begin(); it != elements->end(); it++) {
        if(it->getX() < spinBoxInput->value()) {
            answer += it->getPx();
        }
        
    }

    QString str = QString::number(answer);
    lineAnsver->setText(str);
    
}

int MainWindow::scanTable() {
    elements->clear();

    for(int j = 0; j < spinBox->value(); j++) {
        itemX = new QTableWidgetItem();        
        itemX = table->item(0, j);

        itemY = new QTableWidgetItem();
        itemY = table->item(1, j);
        if (itemX != NULL && itemY != NULL) {
            Element e;

            e.setX(itemX->text().toInt());
            e.setPx(itemY->text().toDouble());

            elements->push_back(e);
        }
        else {
            QMessageBox::warning(this, "Помилка", "присутній пустий елемент");
            return 0;
        }
    }

    return 1;
}

void MainWindow::showGraphic() {
    if(!scanTable()) 
        return;

    int max = 0, min = 100;
    double answer = 0;

    for(auto it = elements->begin(); it != elements->end(); it++) {
        if(it->getX() > max)
            max = it->getX();

        if(it->getX() < min)
            min = it->getX();
        
        answer += it->getPx();
        it->setDpx(answer);
    }
    
    QVector<double> x;
    QVector<double> y;

    min = min - fabs(0.5 * min);
    max = max + fabs(0.5 * max);
    
    x.push_back(min);
    y.push_back(0);
   
    for(auto it = elements->begin(); it != elements->end(); it++) {

        x.push_back(it->getX());

        if(it == elements->begin()) {
            y.push_back(0);
        } else {
            y.push_back((it - 1)->getDpx());
        }

        x.push_back(it->getX());
        y.push_back(it->getDpx());

    }

    auto itx = x.begin();
    auto ity = y.begin();

    while(itx != x.end() && ity != y.end()) {
        std::cout << "x: " << *itx << "\t" << "y: " << *ity << "\n";
        itx++;
        ity++;
    }


    x.push_back(max);
    y.push_back(1);

    wGraphic->clearGraphs();
    wGraphic->addGraph();
    wGraphic->graph()->setData(x, y);

    wGraphic->xAxis->setLabel("x");
    wGraphic->yAxis->setLabel("P(x)");

    wGraphic->xAxis->setRange(min, max);
    wGraphic->yAxis->setRange(0, 1);

    wGraphic->setMinimumSize(800, 800);
    wGraphic->setWindowTitle("Графік");

    wGraphic->show();
}

void MainWindow::changeSpinBox(int argc) {
    table->setColumnCount(argc);
}

MainWindow::~MainWindow() {

}

